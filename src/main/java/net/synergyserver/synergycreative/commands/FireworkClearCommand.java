package net.synergyserver.synergycreative.commands;

import net.synergyserver.synergycore.commands.CommandDeclaration;
import net.synergyserver.synergycore.commands.CommandFlags;
import net.synergyserver.synergycore.commands.SubCommand;
import net.synergyserver.synergycore.configs.Message;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.FireworkMeta;

@CommandDeclaration(
        commandName = "clear",
        permission = "syn.firework.clear",
        usage = "/firework clear",
        description = "Clears the firework in a player's hand to a default one.",
        parentCommandName = "firework"
)
public class FireworkClearCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        PlayerInventory inventory = player.getInventory();
        ItemStack fireworkItem = inventory.getItemInMainHand();
        FireworkMeta fireworkMeta = (FireworkMeta) fireworkItem.getItemMeta();

        if(!fireworkItem.getType().equals(Material.FIREWORK_ROCKET)){
            player.sendMessage(Message.get("commands.firework.error.no_firework"));
            return false;
        }

        fireworkMeta.setPower(0);
        fireworkMeta.clearEffects();
        fireworkItem.setItemMeta(fireworkMeta);

        player.sendMessage(Message.get("commands.firework.clear.info.cleared_firework"));
        return true;
    }
}
