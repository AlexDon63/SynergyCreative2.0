package net.synergyserver.synergycreative.commands;

import net.synergyserver.synergycore.commands.CommandDeclaration;
import net.synergyserver.synergycore.commands.CommandFlags;
import net.synergyserver.synergycore.commands.SenderType;
import net.synergyserver.synergycore.commands.SubCommand;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.ItemUtil;
import net.synergyserver.synergycreative.inputs.Input;
import net.synergyserver.synergycreative.inputs.InputFunction;
import net.synergyserver.synergycreative.inputs.InputManager;
import net.synergyserver.synergycreative.inputs.UserGroup;
import net.synergyserver.synergycreative.inputs.WirelessInputFunction;
import net.synergyserver.synergycreative.utils.PlotUtil;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Set;
import java.util.UUID;

@CommandDeclaration(
        commandName = "wireless",
        aliases = "wl",
        permission = "syn.input.add.wireless",
        usage = "/input add wireless [plot_trusted|plot_members|everyone]",
        description = "Adds wireless activation functionality to the input you are looking at and binds it to your held item." +
                "Players in the given user group can also use the item to activate the input.",
        maxArgs = 1,
        validSenders = SenderType.PLAYER,
        parentCommandName = "input add"
)
public class InputAddWirelessCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        UUID pID = player.getUniqueId();

        // If a user group was given and it isn't valid then give the player an error, otherwise use it
        UserGroup userGroup = UserGroup.OWNER;
        if (args.length == 1) {
            switch (args[0].toLowerCase()) {
                case "plot_trusted":
                case "trusted":
                    userGroup = UserGroup.PLOT_TRUSTED;
                    break;
                case "plot_members":
                case "members":
                    userGroup = UserGroup.PLOT_MEMBERS;
                    break;
                case "everyone":
                case "all":
                    userGroup = UserGroup.EVERYONE;
                    break;
                default:
                    player.sendMessage(Message.format("commands.error.invalid_user_group", args[0]));
                    return false;
            }
        }

        // If the player isn't holding an item then give the player an error
        ItemStack heldItem = player.getInventory().getItemInMainHand();
        if (heldItem == null || heldItem.getType().equals(Material.AIR)) {
            player.sendMessage(Message.format("commands.input.add.wireless.error.no_item_in_hand"));
            return false;
        }

        // If the player isn't looking at an input then give the player an error
        Block block = player.getTargetBlock((Set<Material>) null, 10);
        if (block == null || !Input.isValidInput(block.getType())) {
            player.sendMessage(Message.format("commands.input.error.input_not_found"));
            return false;
        }

        // If the player cannot build where the Input is being created then give the player an error
        if (!PlotUtil.canBuild(player, block.getLocation())) {
            player.sendMessage(Message.format("commands.input.add.error.no_permission"));
            return false;
        }

        // Get the Input at the block's location
        Input input = InputManager.getInstance().getOrCreateInput(block, pID);

        // If they can't modify the input then give the player an error
        if (!PlotUtil.canBuild(player, input.getLocation())) {
            player.sendMessage(Message.format("commands.input.error.no_modify_permission"));
            return false;
        }

        // If the Input already has a WirelessInputFunction registered then give the player an error
        for (InputFunction inputFunction : input.getInputFunctions()) {
            if (inputFunction instanceof WirelessInputFunction) {
                player.sendMessage(Message.format("commands.input.add.error.function_already_exists", inputFunction.getType()));
                return false;
            }
        }

        // If there is already a UUID associated with the itemstack then use it, otherwise add one
        String itemID = ItemUtil.getNBTString(heldItem, "activatorID");
        if (itemID.isEmpty()) {
            itemID = UUID.randomUUID().toString();
            heldItem = ItemUtil.setNBTString(heldItem, "activatorID", itemID);

            // Update the item in the main hand with the new NBT
            player.getInventory().setItemInMainHand(heldItem);
        }

        // Create a WirelessInputFunction and register it to the Input
        WirelessInputFunction wirelessInput = new WirelessInputFunction(input, userGroup, itemID, heldItem.getType());
        input.addInputFunction(wirelessInput);

        // Send the player a success message
        String material = heldItem.getType().name().toLowerCase().replace("_", " ");
        if (userGroup.equals(UserGroup.OWNER)) {
            player.sendMessage(Message.format("commands.input.add.wireless.info.created_no_group", material));
        } else {
            player.sendMessage(Message.format("commands.input.add.wireless.info.created_with_group", material, userGroup.toString()));
        }
        return true;
    }
}
