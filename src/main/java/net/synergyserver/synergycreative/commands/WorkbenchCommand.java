package net.synergyserver.synergycreative.commands;


import net.synergyserver.synergycore.commands.CommandDeclaration;
import net.synergyserver.synergycore.commands.CommandFlags;
import net.synergyserver.synergycore.commands.MainCommand;
import net.synergyserver.synergycore.commands.SenderType;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@CommandDeclaration(
        commandName = "workbench",
        aliases = {"craft", "bench"},
        permission = "syn.workbench",
        usage = "/workbench",
        description = "Opens the 3x3 crafting grid user interface.",
        maxArgs = 0,
        validSenders = SenderType.PLAYER
)

public class WorkbenchCommand extends MainCommand {
    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;

        player.openWorkbench(null, true);

        return true;
    }
}
