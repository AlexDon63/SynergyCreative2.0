package net.synergyserver.synergycreative.settings;

import net.synergyserver.synergycore.settings.SettingCategory;
import net.synergyserver.synergycore.settings.ToggleSetting;
import org.bukkit.Material;

/**
 * Represents a <code>ToggleSetting</code> handled by SynergyCreative.
 */
public class CreativeToggleSetting extends ToggleSetting {

    public static final CreativeToggleSetting DROPS = new CreativeToggleSetting("drops", SettingCategory.CREATIVE,
            "Allows item drops to be created when pressing the item drop key or breaking inventory holders.",
            "syn.setting.drops", true, true, Material.MELON_SEEDS);
    public static final CreativeToggleSetting NIGHT_VISION = new CreativeToggleSetting("night_vision",
            SettingCategory.CREATIVE, "Gives an unlimited night vision potion effect with no particles,",
            "syn.setting.night-vision", false, false, Material.POTION);
    public static final CreativeToggleSetting FLIP_SLABS = new CreativeToggleSetting("flip_slabs",
            SettingCategory.CREATIVE, "Automatically makes slabs flip to the top half of the block when placed.",
            "syn.setting.flip-slabs", false, false, Material.QUARTZ_SLAB);
    public static final CreativeToggleSetting FILL_CAULDRONS = new CreativeToggleSetting("fill_cauldrons",
            SettingCategory.CREATIVE, "Automatically fills cauldrons to the brim when placed.",
            "syn.setting.fill-cauldrons", false, false, Material.CAULDRON);
    public static final CreativeToggleSetting REMOVE_TORCHES_ON_REDSTONE_BLOCKS = new CreativeToggleSetting(
            "remove_torches_on_redstone_blocks", SettingCategory.CREATIVE,
            "Automatically removes redstone torches after they're placed on redstone blocks.",
            "syn.setting.remove-torches-on-redstone-blocks", false, false,
            Material.REDSTONE_TORCH);
    public static final CreativeToggleSetting NOCLIP = new CreativeToggleSetting("noclip", SettingCategory.CREATIVE,
            "Switches your gamemode to spectator whenever you get near a block.", "syn.setting.noclip", false, false,
            Material.GLASS);
//    public static final CreativeToggleSetting WORLDEDIT_ANYWHERE = new CreativeToggleSetting("worldedit_anywhere",
//            SettingCategory.CREATIVE, "Allows WorldEditing everywhere. Use with caution.",
//            "syn.setting.worldedit-anywhere", false, false, Material.WOODEN_AXE);

    private CreativeToggleSetting(String id, SettingCategory category, String description, String permission,
                              boolean defaultValue, boolean defaultValueNoPermission, Material itemType) {
        super(id, category, description, permission, defaultValue, defaultValueNoPermission, itemType);
    }

}
